<?php
namespace supervillainhq\phpbasics\corecomponents{

	class EnterprisePage extends TemplatePage{
		// scripts, stylesheets, etc.
		use Javascripting;
		use Stylesheeting;
		// nested content-containers
		use Sectioning;

		public function __construct(array $data = null){
			$this->resetJavascripts();
			$this->resetStylesheets();
			$this->resetSections();
		}
	}
}
?>