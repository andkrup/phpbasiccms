<?php
namespace supervillainhq\phpbasics\corecomponents{
	use supervillainhq\phpbasics\web\html\templating\Template;
	use supervillainhq\phpbasics\contentmanagement\Page;

	class TemplatePage extends Page{
		protected $template;

		function template(Template $template = null){
			if(is_null($template)){
				return $this->template;
			}
			$this->template = $template;
		}

		public function __construct(array $data = null){
		}
	}
}