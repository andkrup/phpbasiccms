<?php
namespace supervillainhq\phpbasics\contentmanagement{
	use supervillainhq\phpbasics\db\DataAware;
	use supervillainhq\phpbasics\web\Response;

	class Page{
		// has table-related properties
		use DataAware;

		protected $response;
		protected $title;
		protected $slug;
		protected $status;

		function title($title = null){
			if(is_null($title)){
				return $this->title;
			}
			$this->title = $title;
		}
		function response(Response $response = null){
			if(is_null($response)){
				return $this->response;
			}
			$this->response = $response;
		}
		function slug($slug = null){
			if(is_null($slug)){
				return $this->slug;
			}
			$this->slug = $slug;
		}
		function status($status = null){
			if(is_null($status)){
				return $this->status;
			}
			$this->status = $status;
		}
	}
}