<?php
namespace supervillainhq\phpbasics\corecomponents{
	use supervillainhq\phpbasics\db\DataAware;

	class Section {
		use DataAware;

		protected $contents;

		public function __construct(array $data = null){
		}

		public function contents($contents = null){
			if(is_null($contents)){
				return $this->contents;
			}
			$this->contents = $contents;
		}
	}
}
?>