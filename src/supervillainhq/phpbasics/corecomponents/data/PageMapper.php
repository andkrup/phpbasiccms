<?php
namespace supervillainhq\phpbasics\corecomponents\data{
	use supervillainhq\phpbasics\db\Mapper;
	use supervillainhq\phpbasics\contentmanagement\Page;

	class PageMapper implements Mapper{
		protected $page;

		function page(Page $page = null){
			if(is_null($page)){
				return $this->page;
			}
			$this->page = $page;
		}

		function __construct(Page $page = null, array $defaults = null){
			$this->properties($defaults);
		}

		function search(array $parameters = null){}
		function create(array $parameters = null){
			if(is_null($parameters)){
				if(isset($this->page)){
					$parameters = [
							'title' => $this->page->title(),
							'slug' => $this->page->slug(),
							'status' => $this->page->status(),
					];
				}
				elseif(isset($this->properties)){
					$parameters = [
							'title' => $this->properties->title,
							'slug' => $this->properties->slug,
							'status' => $this->properties->status,
					];
				}
			}
			$sql = "insert into Pages (title, slug, status)
					values(%d, %d, %d);";
		}
		function restore(array $identifiers = null){}
		function update($object = null, array $properties = null){}
		function delete($object = null){}
	}
}