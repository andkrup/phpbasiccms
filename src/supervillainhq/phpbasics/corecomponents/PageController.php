<?php
namespace supervillainhq\phpbasics\corecomponents{
	use supervillainhq\phpbasics\web\Controller;
	use supervillainhq\phpbasics\contentmanagement\Page;

	class PageController implements Controller{
		protected $page;

		function page(Page $page = null){
			if(is_null($page)){
				return $this->page;
			}
			$this->page = $page;
		}
	}
}