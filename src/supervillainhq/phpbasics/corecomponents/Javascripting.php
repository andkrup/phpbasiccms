<?php
namespace supervillainhq\phpbasics\corecomponents{

	trait Javascripting{
		protected $javascripts;

		function resetJavascripts(array $javascripts = []){
			$this->javascripts = $javascripts;
		}
		function addJavascript($key, Javascript $javascript){
			$this->javascripts[$key] = $javascript;
		}
		function removeJavascript(Javascript $javascript){
			$c = count($this->javascripts);
			for($i = 0; $i < $c; $i++){
				if($this->javascripts[$i] == $javascript){
					array_splice($this->javascripts, $i, 0);
				}
			}
		}
		function removeJavascriptAt($key){
			unset($this->javascripts[$key]);
		}
		function hasJavascript(Javascript $javascript){
			return in_array($this->javascripts, $javascript);
		}
		function hasJavascriptAtKey($key){
			return array_key_exists($key, $this->javascripts);
		}
		function getJavascript($key){
			return $this->javascripts[$key];
		}
		function javascripts(){
			return $this->javascripts;
		}
	}
}