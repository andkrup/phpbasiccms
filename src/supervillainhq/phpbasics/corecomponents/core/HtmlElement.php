<?php
namespace supervillainhq\phpbasics\corecomponents\core{
	class HtmlElement implements IHtmlElement{
		protected $id;
		protected $tag;
		protected $classes;
		protected $attributes;
		protected $inner;

		public function __construct($tag, $id = null){
			$this->tag = $tag;
			$this->id = $id;
			$this->resetInner();
			$this->clearClasses();
			$this->clearAttributes();
		}

		public function startTag(){
			$id = !isset($this->id) && isset($this->id) ? '':" id=\"$this->id\"";
			$classes = $this->printClasses();
			$attributes = $this->printAttributes();
			return "<$this->tag$id$classes$attributes>\n";

		}
		public function endTag(){
			return "</$this->tag>\n";
		}
		public function openTag(){
			$id = !isset($this->id) || is_null($this->id) ? '':" id=\"$this->id\"";
			$classes = $this->printClasses();
			return "<$this->tag$id$classes />\n";
		}

		public function containsClass($class){
			foreach($this->classes as $item){
				if($class == $item){
					return true;
				}
			}
			return false;
		}
		public function addClass($class){
			if(!$this->containsClass($class)){
				array_push($this->classes, $class);
			}
		}
		public function removeClass($class){
			$c = count($this->classes);
			for($i = 0; $i < $c; $i++){
				$item = $this->classes[$i];
				if($class == $item){
					array_splice($this->classes, $i, 1);
					return;
				}
			}
		}
		public function getClasses(){
			return $this->classes;
		}
		public function getClass($index){
			return $this->classes[$index];
		}
		public function clearClasses(){
			$this->classes = array();
		}
		public function printClasses(){
			if(isset($this->classes) && count($this->classes)>0){
				$classes = implode(' ',$this->classes);
				if(strlen(trim($classes))>0){
					return " class=\"$classes\"";
				}
			}
			return "";
		}

		public function containsAttribute($attribute){
			return array_key_exists($attribute, $this->attributes);
		}
		public function containsAttributeValue($attributeValue){
			foreach($this->attributes as $key => $value){
				if($attributeValue == $value){
					return true;
				}
			}
			return false;
		}
		public function addAttribute($key, $value){
			$this->attributes[$key] = $value;
		}
		public function removeAttribute($key){
			if(array_key_exists($key, $this->attributes)){
				unset($this->attributes[$key]);
			}
		}
		public function getAttributes(){
			return $this->attributes;
		}
		public function getAttribute($key){
			return array($key, $this->attributes[$key]);
		}
		public function getAttributeValue($key){
			return $this->attributes[$key];
		}
		public function clearAttributes(){
			$this->attributes = array();
		}
		protected function printAttributes(){
			$buffer = '';
			if(isset($this->attributes)){
				foreach($this->attributes as $key => $value){
					$buffer .= " $key=\"$value\"";
				}
			}
			return $buffer;
		}

		protected function resetInner(){
			$this->inner = array();
		}
		public function innerContents(){
			$arr = $this->inner;
			$string = '';
			while(count($arr)>0){
				$element = array_shift($arr);
				$string .= "$element\n";
			}
			return $string;
		}

		public function append(HtmlElement $element){
			array_push($this->inner, $element);
		}
		public function prepend(HtmlElement $element){
			array_unshift($this->inner, $element);
		}
		public function appendText($text){
			array_push($this->inner, $text);
		}
		public function prependText($text){
			array_unshift($this->inner, $text);
		}

		public function __toString(){
			$buffer = $this->startTag();
			$buffer .= $this->innerContents();
			$buffer .= $this->endTag();
			return $buffer;
		}
	}
}
