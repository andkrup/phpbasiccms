<?php
namespace supervillainhq\phpbasics\corecomponents\core{
	interface IHtmlElement{
		public function startTag();
		public function endTag();
		public function containsClass($class);
		public function addClass($class);
		public function removeClass($class);
		public function getClasses();
		public function getClass($index);
		public function clearClasses();
		public function printClasses();
		public function containsAttribute($attribute);
		public function containsAttributeValue($attributeValue);
		public function addAttribute($key, $value);
		public function removeAttribute($key);
		public function getAttributes();
		public function getAttribute($key);
		public function getAttributeValue($key);
		public function clearAttributes();
	}
}