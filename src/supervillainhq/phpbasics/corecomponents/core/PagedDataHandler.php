<?php
namespace supervillainhq\phpbasics\corecomponents\core{
	abstract class PagedDataHandler{
		private $lower;
		private $upper;
		private $max;
		private $pagecount;
		protected $current;
		protected $orderby;
		protected $sortDirection;

		public function __construct(){
		}

		public function setSortDirection($value){
			$this->sortDirection = $value == 'desc' ? 'desc' : 'asc';
		}
		public function getSortDirection(){
			return $this->sortDirection;
		}
		abstract public function setOrderBy($value);
		public function getOrderBy(){
			return $this->orderby;
		}

		public function getLower(){
			return $this->lower;
		}
		public function setLower($value){
			$this->lower = $value;
		}
		public function getUpper(){
			return $this->upper;
		}
		public function setUpper($value){
			$this->upper = $value;
		}
		public function getMax(){
			return $this->max;
		}
		public function setMax($max){
			$this->max = $max;
		}
		public function getCurrent(){
			return $this->current;
		}
		public function getPagecount(){
			return $this->pagecount;
		}
		/**
		 * If the upper value isn't set, it will be set to $value.
		 * If the lower value isn't set, it will be set to $value - $value.
		 * @param unknown $value
		 */
		public function setPagecount($value){
			$this->pagecount = $value;
			if(!is_numeric($this->lower)){
				$this->lower = $value - $value;
			}
			if(!is_numeric($this->upper)){
				$this->upper = $value;
			}
		}

		public function reset(){
			$this->current = 0;
			$this->lower = 0;
		}

		public function next(){
			$this->current++;
			if(!is_nan($this->max)){
				if($this->current > $this->max){
					$this->current = $this->max;
				}
			}
			$this->lower = $this->upper;
			$this->upper += $this->pagecount;
		}
		public function previous(){
			$this->current--;
			if($this->current < 0){
				$this->current = 0;
			}
			$this->upper = $this->lower;
			$this->upper -= $this->pagecount;
		}
		public function go($page = 0){
			$this->current = intval($page);
			if(!is_nan($this->max)){
				if($this->current > $this->max){
					$this->current = $this->max;
				}
			}
			$this->lower = intval($this->current) * $this->pagecount;
			$this->upper = $this->lower + $this->pagecount;
		}

		abstract public function loadData();
		abstract public function fetch();
	}
}