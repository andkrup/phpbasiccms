<?php
namespace supervillainhq\phpbasics\corecomponents\core{
	class Iframe extends HtmlElement implements IHtmlElement{
		protected $src;
		protected $context;
		private $buffer;

		public function __construct($src, $context, $id = null){
			parent::__construct('iframe', $id);
			$this->src = $src;
			$this->context = $context;
		}

		public function handleRequest($context){
			if($this->context == $context){
				$this->buffer = $this->__toString();
			}
		}
		public function getContents(){
			return $this->buffer;
		}

		public function __toString(){
			$id = is_null($this->id) ? '' : " id=\"{$this->id}\"";
			$tag = $this->tag;
			$src = " src=\"{$this->src}\"";
			$classes = $this->printClasses();
			$buffer = "<$tag$id$classes$src>";
			$buffer .= "</$tag>";
			return $buffer;
		}
	}
}