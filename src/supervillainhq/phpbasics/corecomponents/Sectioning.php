<?php
namespace supervillainhq\phpbasics\corecomponents{
	trait Sectioning{
		protected $sections;

		function resetSections(array $sections = []){
			$this->sections = $sections;
		}
		function addSection($key, \Section $section){
			$this->sections[$key] = $section;
		}
		function removeSection(\Section $section){
			$c = count($this->sections);
			for($i = 0; $i < $c; $i++){
				if($this->sections[$i] == $section){
					array_splice($this->sections, $i, 0);
				}
			}
		}
		function removeSectionAt($key){
			unset($this->sections[$key]);
		}
		function hasSection(\Section $section){
			return in_array($this->sections, $section);
		}
		function hasSectionAtKey($key){
			return array_key_exists($key, $this->sections);
		}
		function getSection($key){
			return $this->sections[$key];
		}
		function sections(){
			return $this->sections;
		}

	}
}