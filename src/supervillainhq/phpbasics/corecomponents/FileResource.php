<?php
namespace supervillainhq\phpbasics\corecomponents{
	class FileResource{
		protected $dependencies;
		protected $path;

		function path($path = null){
			if(is_null($path)){
				return $this->path;
			}
			$this->path = $path;
		}

		public function __construct($path, array $dependencies = null){
			$this->path = $path;
			$this->clearDependencies();
			if(!is_null($dependencies)){
				foreach($dependencies as $dependency){
					$this->addDependency($dependency);
				}
			}
		}

		public function containsDependency(FileResource $fileResource){
			foreach($this->dependencies as $item){
				if($fileResource == $item){
					return true;
				}
			}
			return false;
		}
		public function addDependency(FileResource $fileResource){
			if(!$this->containsDependency($fileResource)){
				array_push($this->dependencies, $fileResource);
			}
		}
		public function removeDependency(FileResource $fileResource){
			$c = count($this->dependencies);
			for($i = 0; $i < $c; $i++){
				$item = $this->dependencies[$i];
				if($fileResource == $item){
					array_splice($this->dependencies, $i, 1);
					return;
				}
			}
		}
		public function getDependencies(){
			return $this->dependencies;
		}
		public function getDependency($index){
			return $this->dependencies[$index];
		}
		public function clearDependencies(){
			$this->dependencies = array();
		}

		static function printFileResources(FileResource $fileResource){
			$resources = [];
			foreach ($fileResource->dependencies as $dependency){
				$resources = $dependency->import($resources);
			}
			array_push($resources, $fileResource);
			$fileResource->printBuffer($resources);
		}
		protected function printBuffer(array $resources){
			echo "";
		}
	}
}
?>