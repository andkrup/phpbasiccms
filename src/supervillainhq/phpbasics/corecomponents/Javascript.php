<?php
namespace supervillainhq\phpbasics\corecomponents{
	class Javascript extends FileResource{

		protected function printBuffer(array $resources){
			foreach ($resources as $fileResource){
				echo "<script type=\"text/javascript\" src=\"{$fileResource->path()}\"></script>\n";
			}
		}
	}
}
?>