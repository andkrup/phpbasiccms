<?php
namespace supervillainhq\phpbasics\corecomponents{
	use supervillainhq\phpbasics\web\Controller;
	use supervillainhq\phpbasics\web\ApplicationProvider;
	use supervillainhq\phpbasics\web\events\HtmlServiceEvent;
	use supervillainhq\phpbasics\web\html\templating\Template;
	use supervillainhq\phpbasics\contentmanagement\Page;

	class TemplateController implements Controller{
		protected $template;
		protected $data;


		function template(Template $template = null){
			if(is_null($template)){
				return $this->template;
			}
			$this->template = $template;
		}

		function loadTemplate($path){
			$templateFactory = ApplicationProvider::instance()->getProvider('templateFactory', $path);
			$template = $templateFactory->create();
			if(isset($template)){
				$this->template($template);
			}
		}

		function loadData(){
			$this->template->load($this->data);
		}

		function render(){
			if(isset($this->template)){
				echo $this->template->render();
			}
		}

		function onServiceLoadData(HtmlServiceEvent $event){
			$this->loadData();
		}
		function onServiceRender(HtmlServiceEvent $event){
			$this->render();
		}
	}
}