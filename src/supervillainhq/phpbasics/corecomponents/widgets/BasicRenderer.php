<?php
namespace supervillainhq\phpbasics\corecomponents\widgets{
	use supervillainhq\phpbasics\corecomponents\widgets\DefaultWidgetTheme;

	class BasicRenderer{
		function __construct(WidgetTheme $theme = null, array $config = null, array $editOptions = null){
			$this->theme = is_null($theme) ? new DefaultWidgetTheme() : $theme;
			$this->config = $config;
			$this->editOptions = $editOptions;
		}

		protected function getConfigValue($key){
			if(isset($this->config)){
				if(array_key_exists($key, $this->config)){
					return trim($this->config[$key]);
				}
			}
			return '';
		}
		protected function getConfigAsAttribute($key){
			$value = $this->getConfigValue($key);
			if('' != $value){
				return " {$key}=\"{$value}\"";
			}
			return '';
		}
	}
}