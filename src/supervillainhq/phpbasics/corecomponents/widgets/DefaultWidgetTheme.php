<?php
namespace supervillainhq\phpbasics\corecomponents\widgets{
	class DefaultWidgetTheme implements WidgetTheme{
		function getCssClasses($tagname){
			switch ($tagname) {
				case 'table':
					return 'datatable';
			}
			return '';
		}
	}
}