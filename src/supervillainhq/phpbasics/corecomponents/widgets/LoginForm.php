<?php
namespace supervillainhq\phpbasics\corecomponents\widgets{
	use supervillainhq\phpbasics\db\SQLQuery;
	use util\auth\Blowfish;

	class LoginForm{
		protected $renderer;
		protected $dataSource;
		protected $result;
		protected $dataRows;
		protected $username;
		protected $password;

		function __construct($authorized, array $config = null, array $options = null, WidgetRenderer $renderer = null){
			$this->renderer = is_null($renderer) ? new LoginFormHtmlRenderer($authorized, null, $config, $options) : $renderer;
		}


		function dataSource(SQLQuery $value = null){
			if(!is_null($value)){
				$this->dataSource = $value;
			}
			return $this->dataSource;
		}


		function validate($submittedPassword){
			$this->dataSource->execute();
			$row = $this->dataSource->fetchFirstRow();
// 			$hash = Blowfish::crypt($submittedPassword, 10); // replace with password_hash(), using PASSWORD_BCRYPT when php is updated to 5.5
			if(isset($row)){
				$this->result = Blowfish::validate($submittedPassword, $row->hash); // replace with password_verify() when php is updated to 5.5
				return $this->result;
			}
			return false;
		}

		function render(&$buffer = null){
			if(is_null($buffer)){
				$buffer = '';
			}
			$this->renderer->render($buffer);
		}

		function __toString(){
			$buffer = '';
			$this->render($buffer);
			return $buffer;
		}
	}
}