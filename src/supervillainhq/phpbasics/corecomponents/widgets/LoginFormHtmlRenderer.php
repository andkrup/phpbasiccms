<?php
namespace supervillainhq\phpbasics\corecomponents\widgets{
	use supervillainhq\phpbasics\corecomponents\widgets\WidgetRenderer;

	class LoginFormHtmlRenderer extends BasicRenderer implements WidgetRenderer{
		protected $authorized;

		function __construct($authorized, WidgetTheme $theme = null, array $config = null, array $editOptions = null){
			parent::__construct($theme, $config, $editOptions);
			$this->authorized = $authorized;
		}

		function render(&$buffer, array $data = null){
			$id = $this->getConfigAsAttribute('id');
			$name = $this->getConfigValue('name');
			$nameAttr = $this->getConfigAsAttribute('name');
			$method = $this->getConfigAsAttribute('method');
			$action = $this->getConfigAsAttribute('action');
			$cssClasses = $this->theme->getCssClasses('table');

			if($this->authorized){
				$buffer .= "<form{$id}{$method}{$action}{$nameAttr} class=\"{$cssClasses}\">\n";
				$buffer .= "<input type=\"submit\" name=\"{$name}[logout]\" value=\"log out\">\n";
				$buffer .= "</form>\n";
			}
			else{
				$buffer .= "<form{$id}{$method}{$action}{$nameAttr} class=\"{$cssClasses}\">\n";
				$buffer .= "<dl>\n";
				$buffer .= "<dt>Username</dt>\n";
				$buffer .= "<dd>\n";
				$buffer .= "<input type=\"text\" name=\"{$name}[user]\">\n";
				$buffer .= "</dd>\n";
				$buffer .= "<dt>Password</dt>\n";
				$buffer .= "<dd>\n";
				$buffer .= "<input type=\"password\" name=\"{$name}[pass]\">\n";
				$buffer .= "</dd>\n";
				$buffer .= "</dl>\n";
				$buffer .= "<input type=\"submit\" value=\"log in\">\n";
				$buffer .= "</form>\n";
			}
		}
	}
}