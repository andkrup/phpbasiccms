<?php
namespace supervillainhq\phpbasics\corecomponents\widgets\data{
	use supervillainhq\phpbasics\db\SQLQuery;
	use supervillainhq\phpbasics\corecomponents\widgets\WidgetTheme;
	use supervillainhq\phpbasics\corecomponents\widgets\WidgetRenderer;

	class DataTable{
		protected $jQuery;
		protected $renderer;
		protected $dataSource;
		protected $result;
		protected $dataRows;

		function __construct(array $config = null, array $options = null, WidgetRenderer $renderer = null){
			$this->renderer = is_null($renderer) ? new DataTableHtmlRenderer(null, $config, $options) : $renderer;
		}


		function dataSource(SQLQuery $value = null){
			if(!is_null($value)){
				$this->dataSource = $value;
			}
			return $this->dataSource;
		}


		function fetch(){
			$this->result = $this->dataSource->execute();
			$this->dataRows = $this->dataSource->fetchRows();
			return $this->result;
		}

		function render(&$buffer = null){
			if(is_null($buffer)){
				$buffer = '';
			}
			$this->renderer->render($buffer, $this->dataRows);
		}

		function __toString(){
			$buffer = '';
			$this->render($buffer);
			return $buffer;
		}
	}
}