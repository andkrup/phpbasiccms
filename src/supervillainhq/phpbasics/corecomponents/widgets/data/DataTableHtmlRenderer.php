<?php
namespace supervillainhq\phpbasics\corecomponents\widgets\data{
	use supervillainhq\phpbasics\corecomponents\widgets\WidgetRenderer;
	use supervillainhq\phpbasics\corecomponents\widgets\DefaultWidgetTheme;

	class DataTableHtmlRenderer implements WidgetRenderer{
		protected $theme;
		protected $config;
		protected $editOptions;
		protected $data;
		protected $rows;
		protected $cols;

		function __construct(WidgetTheme $theme = null, array $config = null, array $editOptions = null){
			$this->theme = is_null($theme) ? new DefaultWidgetTheme() : $theme;
			$this->config = $config;
			$this->editOptions = $editOptions;
			$this->rows = 0;
			$this->cols = 0;
		}

		private function getConfigValue($key){
			if(isset($this->config)){
				if(array_key_exists($key, $this->config)){
					return trim($this->config[$key]);
				}
			}
			return '';
		}
		private function getConfigHtml($key){
			$value = $this->getConfigValue($key);
			if('' != $value){
				return " {$key}=\"{$value}\"";
			}
			return '';
		}

		private function htmlHeader(&$buffer){
			$buffer .= "<thead>\n";
			$buffer .= "	<tr>\n";
			foreach ($this->cols as $col){
				$buffer .= "	<th>{$col}</th>\n";
			}
			if(isset($this->editOptions)){
				$buffer .= "	<th>Options</th>\n";
			}
			$buffer .= "	</tr>\n";
			$buffer .= "</thead>\n";
		}
		private function htmlBody(&$buffer){
			$buffer .= "<tbody>\n";
			foreach ($this->data as $row){
				$buffer .= "	<tr>\n";
				foreach ($this->cols as $col){
					$val = $row->$col;
					$buffer .= "	<td>{$val}</td>\n";
				}
				if(isset($this->editOptions)){
					$buffer .= "	<td>\n";
					$this->htmlEditOptions($row, $buffer, $this->editOptions);
					$buffer .= "	</td>\n";
				}
				$buffer .= "	</tr>\n";
			}
			$buffer .= "</tbody>\n";
		}
		private function htmlFooter(&$buffer){
			$buffer .= "<tfoot>\n";
			$buffer .= "	<tr>\n";
			foreach ($this->cols as $col){
				$buffer .= "	<th>{$col}</th>\n";
			}
			if(isset($this->editOptions)){
				$buffer .= "	<th>Options</th>\n";
			}
			$buffer .= "	</tr>\n";
			$buffer .= "</tfoot>\n";
		}
		private function htmlEditOptions($data, &$buffer, $editOptions){
			if(property_exists($data, 'id')){
				$id = $data->id;
			}
			foreach ($editOptions as $option => $url){
				$url = sprintf($url, intval($id));
				$buffer .= "<a href=\"{$url}\">{$option}</a>\n";
			}
		}

		function render(&$buffer, array $data = null){
			$this->data = $data;
			$this->rows = count($data);
			if($this->rows>0){
				$this->cols = array_keys(get_object_vars($data[0]));
			}

			$id = $this->getConfigHtml('id');
			$cssClasses = $this->theme->getCssClasses('table');
			$buffer .= "<table{$id} class=\"{$cssClasses}\">\n";
			$this->htmlHeader($buffer);
			$this->htmlBody($buffer);
			$this->htmlFooter($buffer);
			$buffer .= "</table>\n";
		}
	}
}