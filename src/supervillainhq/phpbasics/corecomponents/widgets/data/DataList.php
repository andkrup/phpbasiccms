<?php
namespace com\molamil\widgets\data{
	class DataList{

		public function startTag(){
			return isset($this->element) ? '' : $this->element->startTag();

		}
		public function endTag(){
			return isset($this->element) ? '' : $this->element->endTag();
		}

		public function containsClass($class){
			return $this->element->containsClass($class);
		}
		public function addClass($class){
			$this->element->addClass($class);
		}
		public function removeClass($class){
			$this->element->removeClass($class);
		}
		public function getClasses(){
			return $this->element->getClasses();
		}
		public function getClass($index){
			return $this->element->getClass($index);
		}
		public function clearClasses(){
			$this->element->clearClasses();
		}
		public function printClasses(){
			return $this->element->printClasses();
		}

		public function containsAttribute($attribute){
			return $this->element->containsAttribute($attribute);
		}
		public function containsAttributeValue($attributeValue){
			return $this->element->containsAttributeValue($attributeValue);
		}
		public function addAttribute($key, $value){
			$this->element->addAttribute($key, $value);
		}
		public function removeAttribute($key){
			$this->element->removeAttribute($key);
		}
		public function getAttributes(){
			return $this->element->getAttributes();
		}
		public function getAttribute($key){
			return $this->element->getAttribute($key);
		}
		public function getAttributeValue($key){
			return $this->element->getAttributeValue($key);
		}
		public function clearAttributes(){
			$this->element->clearAttributes();
		}
	}
}
