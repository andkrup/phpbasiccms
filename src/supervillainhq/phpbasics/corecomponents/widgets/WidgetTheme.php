<?php
namespace supervillainhq\phpbasics\corecomponents\widgets{
	interface WidgetTheme{
		function getCssClasses($tagname);
	}
}