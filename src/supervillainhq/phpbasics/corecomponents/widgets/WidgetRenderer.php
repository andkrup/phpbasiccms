<?php
namespace supervillainhq\phpbasics\corecomponents\widgets{
	interface WidgetRenderer{
		function render(&$buffer, array $data = null);
	}
}