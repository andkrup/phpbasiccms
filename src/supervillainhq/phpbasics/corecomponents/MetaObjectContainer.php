<?php
interface MetaObjectContainer{
	public function containsMetaObject(MetaObject $object);
	public function addMetaObject(MetaObject $object);
	public function removeMetaObject(MetaObject $object);
	public function getMetaObjects();
	public function getMetaObject($index);
	public function clearMetaObjects();
}