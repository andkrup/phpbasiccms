<?php
namespace supervillainhq\phpbasics\corecomponents{
	class StyleSheet extends FileResource{
		protected function printBuffer(array $resources){
			echo "<style\">\n";
			foreach ($resources as $resource){
				echo "@import url('{$resource->path()}');\n";
			}
			echo "</style>\n";
		}
	}
}
?>