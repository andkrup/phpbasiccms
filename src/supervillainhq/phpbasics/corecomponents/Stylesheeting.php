<?php
namespace supervillainhq\phpbasics\corecomponents{

	trait Stylesheeting{
		protected $stylesheets;

		function resetStylesheets(array $stylesheets = []){
			$this->stylesheets = $stylesheets;
		}
		function addStylesheet($key, StyleSheet $stylesheet){
			$this->stylesheets[$key] = $stylesheet;
		}
		function removeStylesheet(StyleSheet $stylesheet){
			$c = count($this->stylesheets);
			for($i = 0; $i < $c; $i++){
				if($this->stylesheets[$i] == $stylesheet){
					array_splice($this->stylesheets, $i, 0);
				}
			}
		}
		function removeStylesheetAt($key){
			unset($this->stylesheets[$key]);
		}
		function hasStylesheet(StyleSheet $stylesheet){
			return in_array($this->stylesheets, $stylesheet);
		}
		function hasStylesheetAtKey($key){
			return array_key_exists($key, $this->stylesheets);
		}
		function getStylesheet($key){
			return $this->stylesheets[$key];
		}
		function stylesheets(){
			return $this->stylesheets;
		}

	}
}