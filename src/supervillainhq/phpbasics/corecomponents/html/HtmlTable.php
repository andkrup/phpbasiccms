<?php
namespace supervillainhq\phpbasics\corecomponents\html{
	use supervillainhq\phpbasics\corecomponents\core\HtmlElement;

	class HtmlTable extends HtmlElement{
		protected $header;
		protected $footer;

		public function __construct($id = null){
			parent::__construct('table', $id);
			$this->header = array();
			$this->footer = array();
		}

		public function appendTextToHeader($string){
			array_push($this->header, $string);
		}
		public function appendTextToFooter($string){
			array_push($this->footer, $string);
		}
		public function headerContents(){
			$arr = $this->header;
			$string = '';
			while(count($arr)>0){
				$element = array_shift($arr);
				$string .= "$element\n";
			}
			return "<thead>\n$string</thead>\n";
		}
		public function footerContents(){
			$arr = $this->footer;
			$string = '';
			while(count($arr)>0){
				$element = array_shift($arr);
				$string .= "$element\n";
			}
			return "<tfoot>\n$string</tfoot>\n";
		}

		public function innerContents(){
			return "{$this->headerContents()}{$this->bodyContents()}{$this->footerContents()}";
		}

		public function bodyContents(){
			$arr = $this->inner;
			$string = '';
			while(count($arr)>0){
				$element = array_shift($arr);
				$string .= "$element\n";
			}
			return "<tbody>\n$string</tbody>\n";
		}
	}
}