<?php
namespace supervillainhq\phpbasics\corecomponents\html{
	use supervillainhq\phpbasics\corecomponents\core\HtmlElement;

	class HtmlDD extends HtmlElement{
		public function __construct($id){
			parent::__construct('dd', $id);
		}
	}
}