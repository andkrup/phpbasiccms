<?php
namespace supervillainhq\phpbasics\corecomponents\html{
	use supervillainhq\phpbasics\corecomponents\core\HtmlElement;

	class HtmlDatalist extends HtmlElement{
		public function __construct($id = null){
			parent::__construct('dl', $id);
		}
	}
}
