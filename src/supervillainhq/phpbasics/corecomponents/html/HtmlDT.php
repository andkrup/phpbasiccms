<?php
namespace supervillainhq\phpbasics\corecomponents\html{
	use supervillainhq\phpbasics\corecomponents\core\HtmlElement;

	class HtmlDT extends HtmlElement{
		public function __construct($id){
			parent::__construct('dt', $id);
		}
	}
}