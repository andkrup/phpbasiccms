<?php
namespace supervillainhq\phpbasics\contentmanagement{
	class HttpErrorPage{
		private $errorcode;

		public function __construct($errorcode){
			$this->errorcode = $errorcode;
		}

		public function getTemplate(){
			switch($this->errorcode){
				case 404:
				default:
					return "404.php";
			}
		}
	}
}