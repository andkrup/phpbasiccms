<?php
namespace supervillainhq\phpbasics\corecomponents{
	use supervillainhq\phpbasics\web\Controller;
	use supervillainhq\phpbasics\contentmanagement\TemplatePage;
	use supervillainhq\phpbasics\web\html\templating\Template;

	class TemplatePageController extends TemplateController{
		protected $page;

		function __construct(){
			$mapperFactory = ApplicationProvider::instance()->getProvider('datamapperFactory', Page, ['slug' => '/']);
			$pageMapper = $mapperFactory->create();
			$this->page = $pageMapper->restore();
		}

		function page(TemplatePage $page = null){
			if(is_null($page)){
				return $this->page;
			}
			$this->page = $page;
			if(!is_null($this->template)){
				$this->page->template($this->template);
			}
		}

		function template(Template $template = null){
			if(!is_null($this->page)){
				throw new \Exception('Page does not exist');
			}
			if(is_null($template)){
				return $this->page->template();
			}
			$this->page->template($template);
			parent::template($template);
		}
	}
}