<?php
class MetaTag implements MetaObject{
	const TYPE_JAVASCRIPT = 'javascript';
	const TYPE_STYLESHEET = 'stylesheet';
	const TYPE_METATAG = 'metatag';
	private $dao;
	private $title;
	private $type;
	private $value;
	private $context;
	private $created;
	private $author;

	public function __construct($id){
		$this->dao = new MetaObjectDAO($id);
	}

	public function clientscript(){
		return intval($this->type) == self::TYPE_JAVASCRIPT;
	}
	public function stylesheet(){
		return intval($this->type) == self::TYPE_STYLESHEET;
	}
	public function metatag(){
		return intval($this->type) == self::TYPE_METATAG;
	}

	public function getTitle(){
		return $this->title;
	}
	public function setTitle($value){
		$this->title = $value;
	}
	public function getType(){
		return $this->type;
	}
	public function setType($value){
		$this->type = $value;
	}
	public function getValue(){
		return $this->value;
	}
	public function setValue($value){
		$this->value = $value;
	}
	public function getContext(){
		return $this->context;
	}
	public function setContext($value){
		$this->context = $value;
	}
	public function getCreated(){
		return $this->created;
	}
	public function setCreated($value){
		$this->created = $value;
	}
	public function getAuthor(){
		return $this->author;
	}
	public function setAuthor($value){
		$this->author = $value;
	}

	public static function create($title, $type, $value, $context, $created, $author){
		return MetaObjectDAO::create($title, $type, $value, $context, $created, $author);
	}
	public static function createFromRow(array $row){
		$object = new MetaTag(intval($row['id']));
		$object->setTitle($row['title']);
		$object->setType($row['type']);
		$object->setValue($row['value']);
		$object->setContext($row['context']);
		if(array_key_exists('created', $row)){
			$object->setCreated($row['created']);
		}
		if(array_key_exists('author', $row)){
			$object->setAuthor($row['author']);
		}
		return $object;
	}
	public function getId(){
		return $this->dao->getId();
	}
	public function persist(MetaObject $object){
		return $this->dao->persist($this);
	}
	public function restore(MetaObject &$object){
		return $this->dao->restore($this);
	}
	public function update(array $fields){
		return $this->dao->update($this, $fields);
	}

	public function __toString(){
		switch(intval($this->type)){
			case self::TYPE_JAVASCRIPT:
				$script = new Javascript($this->value);
				return "$script\n";
			case self::TYPE_STYLESHEET:
				$stylesheet = new StyleSheet($this->value);
				return "$stylesheet\n";
			case self::TYPE_METATAG:
				return "{$this->value}\n";
		}
	}
}
?>