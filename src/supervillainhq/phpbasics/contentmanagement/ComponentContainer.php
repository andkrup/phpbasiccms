<?php
namespace supervillainhq\phpbasics\contentmanagement{
	interface ComponentContainer{
		public function containsComponent($context, Component $component);
		public function addComponent($context, Component $component);
		public function removeComponent($context);
		public function getComponents();
		public function getComponent($context);
		public function getComponentsByContext($context);
		public function clearComponents();
	}
}