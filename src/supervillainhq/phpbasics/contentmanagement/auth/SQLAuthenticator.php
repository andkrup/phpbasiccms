<?php
class SQLAuthenticator implements Authenticator{
	private $loginSql;
	private $updateSql;

	/**
	 *
	 * @param String $loginSql must return a count of rows containing the userid
	 * where the password + salt matches.
	 * The validate function will return true when the first column of first row
	 * is higher than 0 (zero).
	 */
	public function __construct($loginSql, $updateSql){
		$this->loginSql = $loginSql;
		$this->updateSql = $updateSql;
	}
	/**
	 * This function uses an internal SQLQuery instance that is created with the
	 * parameters like this:
	 *  $query = new SQLQuery($this->loginSql, $user, md5("$pass$salt"))
	 *
	 * We're assuming that the query will return the hashed password of the matching
	 * user.
	 *
	 * @see Authenticator::validate()
	 */
	public function validate($user, $pass){
		$query = new SQLQuery($this->loginSql, $user, $pass);
		$query->execute();
		$hash = $query->getValue();
		return Blowfish::validate($pass, $hash);
	}

	/**
	 * Updates the password for a user.
	 *
	 * We're assuming that the query updates the row, matching the username.
	 *
	 * @see Authenticator::update()
	 */
	public function update($user, $pass){
		$hash = $this->hash($pass);
		$query = new SQLQuery($this->updateSql, $user, $hash);
		$query->execute();
		return count($query->getResultCode()) >= 0;
	}

	public function hash($pass){
		return Blowfish::crypt($pass, 10);
	}
}
?>