<?php
interface Authenticator{
	/**
	 * The method with which the implementation is hashing a password.
	 * Implementations should at minimum use Auth::blowfishCrypt($pass, $cost).
	 *
	 * @param unknown $pass
	 */
	public function hash($pass);
	/**
	 * Use to validate a usder trying to log in with a password
	 * @param unknown $user
	 * @param unknown $pass
	 *
	 * @return Must return true on success, false on failure
	 */
	public function validate($user, $pass);
	/**
	 * Use to update a database row with a users password
	 * @param unknown $user
	 * @param unknown $pass
	 */
	public function update($user, $pass);
}
?>