<?php
namespace supervillainhq\phpbasics\contentmanagement\auth{
	class AccessManager{
		const EDIT = 'edit';
		const PUBLISH = 'publish';
		const ADMINISTRATION = 'administration';

		public static function isLoggedIn(){
			return isset($_SESSION['user']);
		}

		public static function logout(){
			unset($_SESSION['user']);
		}

		public static function login($user, $pass, Authenticator $authenticator = null){
			if(is_null($authenticator)){
				throw new Exception('Please supply an authenticator');
			}
			if($authenticator->validate($user, $pass)){
				$_SESSION['user'] = $user;
				return true;
			}
			return false;
		}

		public static function allowuser($grant, RestrictedObject $restrictedObject = null){
			if(!self::isLoggedIn()){
				if(is_null($restrictedObject)){
					return true;
				}
				return false;
			}
			// TODO: currently hardcoded roles, rights and accesslevels
			$user = new User($_SESSION['user'], array(self::ADMINISTRATION));
			if(is_null($restrictedObject)){
				return $user->hasGrant($grant);
			}
			else{
				return $user->hasGrant($grant) && $restrictedObject->allowAccess($grant);
			}
		}
	}
}