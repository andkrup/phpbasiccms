<?php
/**
 * Hooks: init.php is included during the constructor
 *
 * @author zr-ank
 *
 */
class BasicTemplatePage extends TemplatePage implements Page{
	private $initfile;
	private $application;

	public function __construct($template, $name, Application $application, $initfile = 'init.php'){
		parent::__construct($template, $name);
		$this->initfile = $initfile;
		$this->application = $application;

		$approot = $this->application->approot();
		$page = $this;
		$site = $this->application->site();
		$path = "{$approot}/templates/{$this->initfile}";
		// allow code injection at template constructor-time
		if(is_file("{$approot}/controllers/{$template}")){
			require "{$approot}/controllers/{$template}";
		}
	}

	public function allowAccess($grant){
		return true;
	}

	public function handleRequest(){
		parent::handleRequest();
	}
}