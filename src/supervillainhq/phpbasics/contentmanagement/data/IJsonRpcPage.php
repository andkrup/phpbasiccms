<?php
interface IJsonRpcPage extends Page{
	public function getJsonRequest();
	public function setJsonRequest($request);
	public function getJsonError();
	public function setJsonError($error);
	public function createJsonResponse();
}