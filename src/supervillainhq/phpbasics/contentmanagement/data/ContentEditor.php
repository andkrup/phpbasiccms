<?php
class ContentEditor{
	private $markupParser;
	
	public function __construct(){
		$this->markupParser = MarkupParserFactory::create();
	}
	
	/**
	 * Takes content marked up in an intermediate markup language and generates
	 * html from it.
	 * 
	 * Use this to store the row that contains the markup. Also update/save the
	 * row that contains the markup for the next editing session. Also update/save
	 * the row that contains the raw contents so markup language can be swapped,
	 * or content can be exported to another system.
	 * 
	 * @param unknown_type $markedupContents
	 */
	public function contentToHtml($markedupContents){
		return $this->markupParser->parse($markedupContents);
	}
}