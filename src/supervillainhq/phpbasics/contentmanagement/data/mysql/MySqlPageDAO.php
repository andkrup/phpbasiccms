<?php
class MySqlPageDAO implements PageDAO{
	private $id;

	public function __construct($id){
		$this->id = $id;
	}

	public function getId(){
		return $this->id;
	}

	/**
	 * XXX: Candidate for caching
	 */
	public function exists(){
		$sql = "select count(id) as cnt from svhq_Objects where id = @(1);";
		$query = new SQLQuery($sql, $this->getId());
		$query->execute();
		return intval($query->getValue('cnt')) > 0;
	}

	public function persist(Page $page){
		return false;
	}
	public function restore(Page &$page){
		// query fetches only the sections with html-contents. Other section-rows contains the markup and raw contents for editing. When editing the row that will be fetched here must be updated with new html-contents
		$sql = "select p.name as page_name, p.template as page_template, p.title as page_title, ps.context, s.id as section_id, s.name as section_name, s_children.contents as section_contents, s.created_datetime as section_created_datetime, s.author_id as section_author
		from svhq_Objects p
		left outer join svhq_ObjectSections ps on ps.object_id = p.id
		left outer join svhq_Sections s on s.id = ps.section_id
		left outer join svhq_Sections s_children on s_children.parent_id = ps.section_id
		where p.id = @(1)
		and s_children.parentrelation_type = 2;";
		$query = new SQLQuery($sql, $page->getId());
		$query->execute();
		$rows = $query->fetchRows();
		if(count($rows) > 0){
			$row = $rows[0];
			$page->setName($row['page_name']);
			$page->setTitle($row['page_title']);
			$page->setTemplate($row['page_template']);
			foreach ($rows as $row){
				$page->addSection(Section::createFromRow($row));
			}
			// also fetch a pages metaobjects (scripts, metatags, stylesheets) - these are additional to the ones hardcoded in the template
			$sql = "select m.id, m.title, m.type, m.value, m.context
			from svhq_ObjectMetas pm
			left outer join svhq_MetaObjects m on m.id = pm.metaobject_id
			where pm.id = @(1);";
			$query = new SQLQuery($sql, $page->getId());
			$query->execute();
			unset($rows);
			$rows = $query->fetchRows();
			if(isset($rows)){
				foreach ($rows as $row){
					$page->addMetaObject(MetaTag::createFromRow($row));
				}
			}
			// also fetch a pages components (external code)
			$sql = "select c.id, c.name, c.classname, c.classpath, pc.context
			from svhq_ObjectComponents pc
			left outer join svhq_Components c on c.id = pc.component_id
			where pc.object_id = @(1);";
			$query = new SQLQuery($sql, $page->getId());
			$query->execute();
			unset($rows);
			$rows = $query->fetchRows();
			$appRoot = Site::approot();
			if(isset($rows)){
				foreach ($rows as $row){
					$clspath = $row['classpath'];
					$clsname = $row['classname'];
					$context = $row['context'];
					require_once "$appRoot/$clspath/$clsname.php";
					$refclassobject = new ReflectionClass($clsname);
					$component = $refclassobject->newInstance();
					$page->addComponent($context, $component);
				}
			}
			return true;
		}
		return false;
	}
}