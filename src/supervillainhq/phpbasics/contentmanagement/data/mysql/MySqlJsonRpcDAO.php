<?php
class MySqlJsonRpcDAO implements PageDAO{
	private $id;

	public function __construct($id){
		$this->id = $id;
	}

	public function getId(){
		return $this->id;
	}

	/**
	 * XXX: Candidate for caching
	 */
	public function exists(){
		$sql = "select count(id) as cnt from svhq_Objects where id = @(1);";
		$query = new SQLQuery($sql, $this->getId());
		$query->execute();
		return intval($query->getValue('cnt')) > 0;
	}

	public function persist(Page $page){
		return false;
	}
	public function restore(Page &$page){
		// query fetches only the sections with html-contents. Other section-rows contains the markup and raw contents for editing. When editing the row that will be fetched here must be updated with new html-contents
		$sql = "select p.template as page_template
		from svhq_Objects p
		where p.id = @(1);";
		$query = new SQLQuery($sql, $page->getId());
		$query->execute();
		$rows = $query->fetchRows();
		if(count($rows) > 0){
			$row = $rows[0];
			$page->setTemplate($row['page_template']);
			// also fetch a pages components (external code)
			$sql = "select c.id, c.name, c.classname, c.classpath, pc.context
			from svhq_ObjectComponents pc
			left outer join svhq_Components c on c.id = pc.component_id
			where pc.object_id = @(1);";
			$query = new SQLQuery($sql, $page->getId());
			$query->execute();
			unset($rows);
			$rows = $query->fetchRows();
			$appRoot = Site::approot();
			if(isset($rows)){
				foreach ($rows as $row){
					$clspath = $row['classpath'];
					$clsname = $row['classname'];
					$context = $row['context'];
					require_once "$appRoot/$clspath/$clsname.php";
					$refclassobject = new ReflectionClass($clsname);
					$component = $refclassobject->newInstance();
					$page->addComponent($context, $component);
				}
			}
			return true;
		}
		return false;
	}
}