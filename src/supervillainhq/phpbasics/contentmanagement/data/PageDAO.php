<?php
interface PageDAO{
	public function getId();
	public function persist(Page $page);
	public function restore(Page &$page);
}
?>