<?php
class JsonRpcTemplagePage extends TemplatePage implements IJsonRpcPage{
	private $request;
	private $error;

	public function __construct(Application $application, $template){
		parent::__construct($template, 'jsonrpcpage');
		
		// load definitions for handling json-rpc
		$appRoot = $application->approot();
		require_once "$appRoot/lib/PhpBasics-jsonrpc.phar";
		
		// handle json data
		$request = JsonRequest::fromString(file_get_contents("php://input"));
		if(isset($request)){
			$this->setJsonRequest($request);
		}
		else{
			$error = new JsonError();
			$error->setCode(-32700);
			$error->setMessage('Parse error. Not well formed');
			$this->setJsonError($error);
		}
		// creating some convenience variables for using in template scope
		$site = $application->site();
		$approot = $application->approot();
		$path = "{$approot}/templates";
		$page = $this;
		// allow code injection at template constructor-time
		if(is_file("{$approot}/controllers/{$template}")){
			require "{$approot}/controllers/{$template}";
		}
	}

	public function getJsonRequest(){
		return $this->request;
	}
	public function setJsonRequest($request){
		$this->request = $request;
	}
	public function getJsonError(){
		return $this->error;
	}
	public function setJsonError($error){
		$this->error = $error;
	}
	public function createJsonResponse(){
		$response = new JsonResponse($this->request);
		if(isset($this->error)){
			$response->setError($this->error);
		}
		return $response;
	}

	public function handleRequest(){
		foreach($this->components as $context=>$component){
			try{
				$component->handleRequest($context, $this);
			}
			catch(JsonRpcException $exception){
				$error = new JsonError();
				$error->setCode($exception->getCode());
				$error->setMessage($exception->getMessage());
				$error->setData(json_encode($exception->getData()));
				$this->setJsonError($error);
				return;
			}
			catch(Exception $exception){
				$error = new JsonError();
				$error->setCode($exception->getCode());
				$error->setMessage($exception->getMessage());
				$this->setJsonError($error);
				return;
			}
		}
	}
}