<?php
class AdminJsonRpcPage implements IJsonRpcPage{
	const DEFAULT_REQUESTKEY = "com";
	private $application;
	private $siteManager;
	private $editor;
	private $template;
	private $components;
	private $command;
	private $request;
	private $error;

	public function __construct(Application $application, $template){
		// load definitions for handling json-rpc
		$appRoot = $application->approot();
		require_once "$appRoot/lib/PhpBasics-jsonrpc.phar";
		
		// handle json data
		$request = JsonRequest::fromString(file_get_contents("php://input"));
		if(isset($request)){
			$this->setJsonRequest($request);
		}
		else{
			$error = new JsonError();
			$error->setCode(-32700);
			$error->setMessage('Parse error. Not well formed');
			$this->setJsonError($error);
		}
		
		$this->application = $application;
		$this->siteManager = new SiteManager();
		$this->siteManager->setApplication($application);
		$this->template = $template;
		$this->clearComponents();
		// administration features
		if(array_key_exists('user', $_GET)){
			// enable wysiwyg editing - if admin is logged in TODO!
			if(AccessManager::allowuser($this->currentPage, AccessManager::EDIT)){
				$this->editor = new ContentEditor();
			}
		}
		// creating some convenience variables for using in template scope
		$site = $application->site();
		$approot = $application->approot();
		$path = "{$approot}/templates/admin";
		$page = $this;
		$sitemanager = $this->siteManager;
		// allow code injection at template constructor-time
		if(is_file("{$approot}/controllers/{$template}")){
			require "{$approot}/controllers/{$template}";
		}
	}

	public function getId(){
		return $this->dao->getId();
	}

	public function getJsonRequest(){
		return $this->request;
	}
	public function setJsonRequest($request){
		$this->request = $request;
	}
	public function getJsonError(){
		return $this->error;
	}
	public function setJsonError($error){
		$this->error = $error;
	}
	public function createJsonResponse(){
		$response = new JsonResponse($this->request);
		if(isset($this->error)){
			$response->setError($this->error);
		}
		return $response;
	}
		
	public function getTitle(){}
	public function getScripts(){}
	public function getSectionsByContext($context){}
	public function getTemplate(){
		return $this->template;
	}
	public function setTemplate($value){
		$this->template = $value;
	}

	public function getCommand(){
		return $this->command;
	}
	public function setCommand(Command $value){
		$this->command = $value;
	}

	public function containsComponent($context, Component $component){
		if(!array_key_exists($context, $this->components)){
			return false;
		}
		return $this->components[$context] == $component;
	}
	public function addComponent($context, Component $component){
		if(!$this->containsComponent($context, $component)){
			$this->components[$context] = $component;
		}
	}
	public function removeComponent($context){
		unset($this->components[$context]);
	}
	public function getComponents(){
		return $this->components;
	}
	public function getComponent($context){
		return $this->components[$context];
	}
	public function getComponentsByContext($context){
		$components = array();
		foreach($this->components as $ctext=>$component){
			if($context == $ctext){
				array_push($components, $component);
			}
		}
		return $components;
	}
	public function clearComponents(){
		$this->components = array();
	}
	public function getHtmlHeadContents(){
		return '';
	}
	public function getHtmlBodyContents(){
		return '';
	}

	public function handleRequest(){
		$request = $this->request;
		foreach($this->components as $context=>$component){
			$component->handleRequest($context);
		}
		// TODO: this flow (setting and executing commands) needs to be re-examined
		// first ask the siteManager for default commands
		$this->command = $this->siteManager->createCommand();
		// then ask for domain-specific commands
		if(!isset($this->command)){
			$this->command = AdminCommandFactory::getInstance()->createCommand();
		}
	}

	public function allowAccess($grant){
		return true;
	}
}