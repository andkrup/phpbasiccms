<?php
class JsonRpcPage extends SimpleRequestHandler implements IJsonRpcPage{
	const DEFAULT_REQUESTKEY = "page";
	private $dao;
	private $template;
	private $components;
	private $request;
	private $error;

	public function __construct(PageDAO $dao){
		$this->dao = $dao;
		$this->clearComponents();
	}

	public function getId(){
		return $this->dao->getId();
	}

	public function getTitle(){}
	public function getScripts(){}
	public function getSectionsByContext($context){}
	public function getTemplate(){
		return $this->template;
	}
	public function setTemplate($value){
		$this->template = $value;
	}

	public function getJsonRequest(){
		return $this->request;
	}
	public function setJsonRequest($request){
		$this->request = $request;
	}
	public function getJsonError(){
		return $this->error;
	}
	public function setJsonError($error){
		$this->error = $error;
	}
	public function createJsonResponse(){
		$response = new JsonResponse($this->request);
		if(isset($this->error)){
			$response->setError($this->error);
		}
		return $response;
	}
		
	public function containsComponent($context, Component $component){
		if(!array_key_exists($context, $this->components)){
			return false;
		}
		return $this->components[$context] == $component;
	}
	public function addComponent($context, Component $component){
		if(!$this->containsComponent($context, $component)){
			$this->components[$context] = $component;
		}
	}
	public function removeComponent($context){
		unset($this->components[$context]);
	}
	public function getComponents(){
		return $this->components;
	}
	public function getComponent($context){
		return $this->components[$context];
	}
	public function getComponentsByContext($context){
		$components = array();
		foreach($this->components as $ctext=>$component){
			if($context == $ctext){
				array_push($components, $component);
			}
		}
		return $components;
	}
	public function clearComponents(){
		$this->components = array();
	}

	public static function getPage($requestKey = null){
		if(is_null($requestKey)){
			$requestKey = self::DEFAULT_REQUESTKEY;
		}
		if(array_key_exists($requestKey, $_GET)){
			$pageid = intval(trim($_GET[$requestKey]));
		}
		if(!isset($pageid)){
			$pageid = 1;
		}
		if(DbConnector::current()->getConnectorType() == DataConnectors::MYSQL){
			$pagedao = new MySqlJsonRpcDAO($pageid);
		}
		if(isset($pagedao) && $pagedao->exists()){
			$page = new JsonRpcPage($pagedao);
			$page->restore();
			return $page;
		}
		return null;
	}

	public function handleRequest(){
		foreach($this->components as $context=>$component){
			$component->handleRequest($context);
		}
	}

	public function persist(){
		$this->dao->persist($this);
	}
	public function restore(){
		$this->dao->restore($this);
	}

	public function allowAccess($grant){
		return true;
	}
}