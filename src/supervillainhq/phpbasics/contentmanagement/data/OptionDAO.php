<?php
interface OptionDAO{
	public function getId();
	public function persist(Option $option);
	public function restore(Option &$option);
}
?>