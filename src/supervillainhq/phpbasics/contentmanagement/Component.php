<?php
namespace supervillainhq\phpbasics\contentmanagement{
	interface Component{
		public function handleRequest($context, $page = null, $application = null);
		public function getContents();
		public function id();
	}
}
?>