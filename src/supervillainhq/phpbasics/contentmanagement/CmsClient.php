<?php
namespace supervillainhq\phpbasics\contentmanagement{
	use supervillainhq\phpbasics\corecomponents\Plugin;
	use supervillainhq\phpbasics\corecomponents\PageFactory;
	use supervillainhq\phpbasics\contentmanagement\cache\PageCache;

	class CmsClient implements Plugin{
		use ComponentContaining;

		protected $pageCache;
		protected $apc;
		protected $pageFactory;

		function pageCache(PageCache $pageCache = null){
			if(is_null($pageCache)){
				return $this->pageCache;
			}
			$this->pageCache = $pageCache;
		}
		function pageFactory(PageFactory $factory = null){
			if(is_null($factory)){
				return $this->pageFactory;
			}
			$this->pageFactory = $factory;
		}

		public function __construct(PageFactory $factory, PageCache $pageCache = null){
			$this->pageFactory($factory);
			$this->pageCache($pageCache);
		}

		public function fetchPage($id){
		}
		public function findPageByTitle($title){
		}
		public function findPageBySlug($slug){
		}

		static function generateSlug($title){
			// replace non letter or digits by -
			$slug = preg_replace('~[^\\pL\d]+~u', '-', $title);
			$slug = trim($slug, '-');
			// transliterate
			$slug = iconv('utf-8', 'us-ascii//TRANSLIT', $slug);
			// lowercase
			$slug = strtolower($slug);
			// remove unwanted characters
			$slug = preg_replace('~[^-\w]+~', '', $slug);

			if(!empty($slug)){
				return $slug;
			}
			return null;
		}
	}
}
