<?php
class MarkupParserFactory{
	public static function create(){
		require_once dirname(__FILE__)."/markdown/MarkdownMarkupParser.php";
		return new MarkdownMarkupParser();
	}
}
?>