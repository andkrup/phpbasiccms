<?php
require_once dirname(__FILE__)."/markdown.php";

/**
 * See http://michelf.ca/projects/php-markdown/
 * 
 * @author anders
 */
class MarkdownMarkupParser implements MarkupParser{
	public function parse($contents){
		return Markdown($contents);
	}
}
?>