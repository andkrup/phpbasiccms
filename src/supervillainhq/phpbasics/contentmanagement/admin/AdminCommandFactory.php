<?php
namespace supervillainhq\phpbasics\admincomponents{

	class AdminCommandFactory implements CommandFactory{
		private static $inst;
		private $factory;

		public static function getInstance(){
			if(!isset(self::$inst)){
				self::$inst = new AdminCommandFactory();
			}
			return self::$inst;
		}

		private function __construct(){
		}

		public function getFactory(){
			return $this->factory;
		}
		public function setFactory(CommandFactory $factory){
			$this->factory = $factory;
		}

		public function createCommand(){
			if(isset($this->factory)){
				return $this->factory->createCommand();
			}
			return null;
		}
	}
}