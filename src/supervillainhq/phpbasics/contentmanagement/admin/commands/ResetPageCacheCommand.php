<?php
class ResetPageCacheCommand implements Command{
	private $filecache;
	private $pageId;
	private $getArray;
	private $application;

	public function __construct(Application $application, $pageId, array $getArray){
		$cacheDir = $application->getCacheDir();
		$this->filecache = new FileCache($cacheDir);
		$this->pageId = $pageId;
		$this->getArray = $getArray;
		$this->application = $application;
	}

	public function execute(){
		$page = $this->application->getPage($this->getArray);
		$page->handleRequest();
		$template = $page->getTemplate();
		$approot = $this->application->approot();
		$path = "{$approot}/templates/$template";
		$this->filecache->create($this->application, $page, $path, $this->getArray);
	}

	public function __toString(){
		return "cached: ".serialize($this->getArray);
	}
}