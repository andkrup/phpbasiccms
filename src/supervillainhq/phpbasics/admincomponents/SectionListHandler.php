<?php
class SectionListHandler extends PagedDataHandler implements DataHandler{
	private $rows;

	public function __construct(){
		$this->setLower(0);
		$this->setPagecount(20);
	}

	public function setOrderBy($value){
		$this->orderby = '';
	}

	public function loadData(){
		$sql = "select
id as section_id,
name as section_name,
created_datetime as section_created
from svhq_Sections s
where parentrelation_type is null
limit @(1), @(2);";
		$query = new SQLQuery($sql, $this->getLower(), $this->getUpper());
		$query->execute();
		$this->rows = $query->fetchRows();
	}

	public function fetch(){
		return $this->rows;
	}
}