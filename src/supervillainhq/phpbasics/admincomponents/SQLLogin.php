<?php
class SQLLogin extends Login{
	public function __construct($id, $loginSql = "select passhash from Users where username = '@(1)';", $updateSql = "update Users set passhash '@(2)' where username = '@(1)';"){
		// tests server env var for a setting that indicates the purpose of the virtual host
		if(Site::isProductionEnv()){
			// will immidiately redirect via a http-header location if the page is not requested and served via https
			Site::requireHttps();
		}
		$authenticator = new SQLAuthenticator($loginSql, $updateSql);
		parent::__construct($id, $authenticator);
	}

	/**
	 * TODO: should be removed when we're confident blowfish crypt works - or moved
	 * to admin-area
	 *
	 * @param unknown $pass
	 */
	public function update($user, $pass){
		$this->authenticator->update($user, $pass);
	}

	/**
	 * TODO: should be removed when we're confident blowfish crypt works
	 * @param unknown $pass
	 */
	public function hash($pass){
		return $this->authenticator->hash($pass);
	}
}
?>