<?php
class PageListHandler extends PagedDataHandler implements DataHandler{
	private $rows;

	public function __construct(){
		$this->setLower(0);
		$this->setPagecount(20);
	}

	public function setOrderBy($value){
		$this->orderby = '';
	}

	public function loadData(){
		$sql = "select
p.id as page_id,
p.name as page_name,
p.template as page_template,
p.title as page_title,
ps.context as section_context,
s.id as section_id,
s.name as section_name,
pc.context as component_context,
c.name as component_name,
c.classname as component_classname
from svhq_Objects p
left outer join svhq_ObjectSections ps on p.id = ps.object_id
left outer join svhq_Sections s on s.id =  ps.section_id
left outer join svhq_ObjectComponents pc on p.id = pc.object_id
left outer join svhq_Components c on c.id =  pc.component_id
limit @(1), @(2);";
		$query = new SQLQuery($sql, $this->getLower(), $this->getUpper());
		$query->execute();
		$this->rows = $query->fetchRows();
	}

	public function fetch(){
		$pages = array();
		$currentPage = array('id'=>$this->rows[0]['page_id'], 'name'=>$this->rows[0]['page_name'], 'template'=>$this->rows[0]['page_template'], 'title'=> $this->rows[0]['page_title']);
		$currentSectionContext = null;
		$currentSectionContexts = array();
		$currentSection = null;
		$currentSections = array();
		$currentComponent = null;
		$currentComponentContext = null;
		$currentComponentContexts = array();
		$currentComponents = array();

		foreach($this->rows as $row){
			$pid = $row['page_id'];
			$pname = $row['page_name'];
			$ptitle = $row['page_title'];
			$ptemplate = $row['page_template'];
			$scontext = $row['section_context'];
			$sname = $row['section_name'];
			$ccontext = $row['component_context'];
			$cname = $row['component_name'];

			if($pname != $currentPage['name']){
				$currentPage['section_contexts'] = $currentSectionContexts;
				$currentPage['component_contexts'] = $currentComponentContexts;
				array_push($pages, $currentPage);
				// then we craete a new instance instead of filling existing  instance up with sub-content objects
				$currentPage = array('name'=>$pname, 'template'=>$ptemplate, 'title'=> $ptitle, 'id' => $pid);
				// force new instances of sub-dimensional objects
				$currentSectionContexts = array();
				$currentSections = array();
				$currentComponentContexts = array();
				$currentComponents = array();
			}

			if($currentSection != $sname){
				array_push($currentSections, $sname);
				$currentSection = $sname;
			}

			if($currentComponent != $cname){
				array_push($currentComponents, $cname);
				$currentComponent = $cname;
			}

			if($currentSectionContext != $scontext){
				$currentSectionContexts[$scontext] = $currentSections;
				$currentSectionContext = $scontext;
				$currentSections = array();
			}

			if($currentComponentContext != $ccontext){
				$currentComponentContexts[$ccontext] = $currentComponents;
				$currentComponentContext = $ccontext;
				$currentComponents = array();
			}
		}
		$currentPage['section_contexts'] = $currentSectionContexts;
		$currentPage['component_contexts'] = $currentComponentContexts;
		array_push($pages, $currentPage);
		return $pages;
	}
}